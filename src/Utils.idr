module Utils

import Lightyear.Core
import Lightyear.Combinators
import Lightyear.Strings
import Lightyear.Char

--scientific stuf from idris-config/Utils  
record Scientific where
  constructor MkScientific
  coefficient : Integer
  exponent : Integer

scientificToFloat : Scientific -> Double
scientificToFloat (MkScientific c e) = fromInteger c * exp
  where
    exp = if e < 0
            then 1 / pow 10 (fromIntegerNat (- e))
            else pow 10 (fromIntegerNat e)

parseScientific : Parser Scientific
parseScientific = do
    sign <- maybe 1 (const (-1)) `map` opt (char '-')
    digits <- some digit
    hasComma <- isJust `map` opt (char '.')
    decimals <- if hasComma
                then some digit
                else pure Prelude.List.Nil
    hasExponent <- isJust `map` opt (char 'e')
    exponent <- if hasExponent
                then integer
                else pure 0
    pure $ MkScientific (sign * fromDigits (digits ++ decimals)) (exponent - cast (length decimals))
  where
    fromDigits : List (Fin 10) -> Integer
    fromDigits = foldl (\a, b => 10 * a + cast b) 0
--

export double : Parser Double
double = scientificToFloat <$> parseScientific

export literal : String -> a -> Parser a
literal name obj = token name *> pure obj

export either : Parser a -> Parser b -> Parser $ Either a b
either ap bp = Left <$> ap <|> Right <$> bp
