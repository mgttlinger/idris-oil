module OIL

%access public export

NEL : Type -> Type
NEL ty = (l: List ty ** NonEmpty l)

Description : Type
Description = Maybe String

AutoSpecifier : Type
AutoSpecifier = Bool

MultipleSpecifier : Type
MultipleSpecifier = Bool

Name : Type
Name = String

record OilVersion where
  constructor MkOilVersion
  version : String
  description : Description

data Default t = NoDefault 
               | Auto 
               | DefaultA t

data NumberRange = BoundedNum Integer Integer 
                 | Of (NEL Integer)
                 
data FloatRange = BoundedFloat Double Double

data Object = OS | Task | Counter | Alarm | Resource | Event | ISR | Message | COM | NM | AppMode | IPDU

data IntegerType = Uint32 | Uint64 | Int32 | Int64

AttributeName : Type
AttributeName = Either Name Object

mutual
  data AttributeValue = SName Name 
                      | CName Name (List Parameter) 
                      | SBool Bool 
                      | SNum Integer 
                      | SFloat Double
                      | SString String 
                      | AutoAttribute

  record Parameter where
    constructor MkParameter
    name : Either Name Object
    value : AttributeValue
    description : Description

ObjectName : Type
ObjectName = (Object, Name)

data ObjectDefinition = SimpleObj ObjectName Description 
                      | ComplexObj ObjectName (List Parameter) Description

record ApplicationDef where
  constructor MkApplicationDef
  name : Name
  definitions : List ObjectDefinition
  description : Description

data ObjectRefType = RefTo Object

record ImplRefDef where
  constructor MkImplRefDef
  objRefType : ObjectRefType
  refName : Either Name Object
  multiSpec : MultipleSpecifier
  description : Description


mutual
  data Enumerator = SimpleEnum Name Description 
                  | ComplexEnum Name (List ImplementationDef) Description

  record BoolValues where
    constructor MkBoolValues
    trueParams : List ImplementationDef
    trueDescription : Description
    falseParams : List ImplementationDef
    falseDescription : Description

 
  data ImplAttrDef : Type where
    IntAtt : IntegerType -> AutoSpecifier -> Maybe NumberRange -> AttributeName -> MultipleSpecifier -> Maybe $ Default Integer -> Description -> ImplAttrDef
    FloatAtt : AutoSpecifier -> Maybe FloatRange -> AttributeName -> MultipleSpecifier -> Maybe $ Default Double -> Description -> ImplAttrDef
    EnumAtt : AutoSpecifier -> (NEL Enumerator) -> AttributeName -> MultipleSpecifier -> Maybe $ Default Name -> Description -> ImplAttrDef
    StringAtt : AutoSpecifier -> AttributeName -> MultipleSpecifier -> Maybe $ Default String -> Description -> ImplAttrDef
    BoolAtt : AutoSpecifier -> Maybe BoolValues -> AttributeName -> MultipleSpecifier -> Maybe $ Default Bool -> Description -> ImplAttrDef  
 
  data ImplementationDef = AttributeDef ImplAttrDef 
                         | ReferenceDef ImplRefDef 
  
record ImplementationSpec where
  constructor MkImplementationSpec
  object : Object
  implementations : List ImplementationDef
  description : Description

record ImplementationDefinition where
  constructor MkImplementationDefinition
  name : Name
  implementationSpecs : (NEL ImplementationSpec)
  description : Description
  
  
record OilFile where
  constructor MkOilFile
  version : OilVersion
  implementationDef : ImplementationDef
  applicationDef : ApplicationDef
  
