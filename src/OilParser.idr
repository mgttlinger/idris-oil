module OilParser

import OIL
import Lightyear.Core
import Lightyear.Combinators
import Lightyear.Strings
import Lightyear.Char
import Utils

stringP : Parser String
stringP = quoted '\"'

booleanP : Parser Bool
booleanP = literal "TRUE" True <|> literal "FALSE" False

nelP : Parser t -> Parser $ NEL t
nelP tp = (brackets $ commaSep1 tp) >>= listToNel
  where 
    listToNel : (Monad m, Alternative m) => List t -> m $ NEL t
    listToNel [] = empty
    listToNel (x :: xs) = pure $ MkDPair (x :: xs) IsNonEmpty


descriptionP : Parser Description
descriptionP = opt (colon *> stringP)

autoSpecifierP : Parser AutoSpecifier
autoSpecifierP = isJust <$> opt (token "WITH_AUTO")

multipleSpecifierP : Parser MultipleSpecifier
multipleSpecifierP = isJust <$> opt (token "[" *> token "]")

nameP : Parser Name
nameP = pack <$> (many alphaNum)

oilVersionP : Parser OilVersion
oilVersionP = token "OIL_VERSION" *> equals *> [| MkOilVersion stringP descriptionP |] <* semi

defaultP : Parser t -> Parser $ Default t
defaultP tp = equals *> (literal "NO_DEFAULT" NoDefault <|> literal "AUTO" Auto <|> DefaultA <$> tp) 

numberRangeP : Parser NumberRange
numberRangeP = (Of <$> nelP integer) <|> (brackets $ do
  start <- integer
  token ".."
  end <- integer
  pure $ BoundedNum start end)
  


floatRangeP : Parser FloatRange
floatRangeP = brackets $ do
  start <- double
  token ".."
  end <- double
  pure $ BoundedFloat start end
  
objectP : Parser Object
objectP = literal "OS" OS 
  <|> literal "TASK" Task 
  <|> literal "COUNTER" Counter
  <|> literal "ALARM" Alarm
  <|> literal "RESOURCE" Resource
  <|> literal "EVENT" Event
  <|> literal "ISR" ISR
  <|> literal "MESSAGE" Message
  <|> literal "COM" COM
  <|> literal "NM" NM
  <|> literal "APPMODE" AppMode
  <|> literal "IPDU" IPDU

integerTypeP : Parser IntegerType
integerTypeP = literal "UINT32" Uint32 <|> literal "UINT64" Uint64 <|> literal "INT32" Int32 <|> literal "INT64" Int64

attributeNameP : Parser AttributeName
attributeNameP = either nameP objectP

mutual
  attributeValueP : Parser AttributeValue
  attributeValueP = SName <$> nameP 
    <|> [| CName nameP (some parameterP) |]
    <|> SBool <$> booleanP
    <|> SNum <$> integer
    <|> SFloat <$> double
    <|> SString <$> stringP
    <|> literal "AUTO" AutoAttribute
  
  parameterP : Parser Parameter
  parameterP = do
    name <- either nameP objectP
    equals
    value <- attributeValueP
    description <- descriptionP
    semi
    pure $ MkParameter name value description

objectNameP : Parser ObjectName
objectNameP = [| MkPair objectP nameP |]

objectDefinitionP : Parser ObjectDefinition
objectDefinitionP = ([| SimpleObj objectNameP descriptionP |] <|> [| ComplexObj objectNameP (braces $ some parameterP) descriptionP |]) <* semi

applicationDefP : Parser ApplicationDef
applicationDefP = token "CPU" *> [| MkApplicationDef nameP (braces $ some objectDefinitionP) descriptionP |] <* semi

objectRefTypeP : Parser ObjectRefType
objectRefTypeP = RefTo <$> (literal "OS_TYPE" OS 
  <|> literal "TASK_TYPE" Task 
  <|> literal "COUNTER_TYPE" Counter
  <|> literal "ALARM_TYPE" Alarm
  <|> literal "RESOURCE_TYPE" Resource
  <|> literal "EVENT_TYPE" Event
  <|> literal "ISR_TYPE" ISR
  <|> literal "MESSAGE_TYPE" Message
  <|> literal "COM_TYPE" COM
  <|> literal "NM_TYPE" NM
  <|> literal "APPMODE_TYPE" AppMode
  <|> literal "IPDU_TYPE" IPDU)

implRefDefP : Parser ImplRefDef
implRefDefP = [| MkImplRefDef objectRefTypeP (either nameP objectP) multipleSpecifierP descriptionP |] <* semi

mutual
  enumeratorP : Parser Enumerator
  enumeratorP = [| SimpleEnum nameP descriptionP |] <|> [| ComplexEnum nameP (braces $ some implementationDefP) descriptionP |]
  
  boolValuesP : Parser BoolValues
  boolValuesP = brackets $ do
    token "TRUE"
    trueParams <- some implementationDefP
    trueDesc <- descriptionP
    comma
    token "FALSE"
    falseParams <- some implementationDefP
    falseDesc <- descriptionP
    pure $ MkBoolValues trueParams trueDesc falseParams falseDesc
  
  implAttrDefP : Parser ImplAttrDef
  implAttrDefP = ([| IntAtt integerTypeP autoSpecifierP (opt numberRangeP) attributeNameP multipleSpecifierP (opt $ defaultP integer) descriptionP |] 
    <|> (token "FLOAT" *> [| FloatAtt autoSpecifierP (opt floatRangeP) attributeNameP multipleSpecifierP (opt $ defaultP double) descriptionP |])
    <|> (token "ENUM" *> [| EnumAtt autoSpecifierP (nelP enumeratorP) attributeNameP multipleSpecifierP (opt $ defaultP nameP) descriptionP |]) 
    <|> (token "STRING" *> [| StringAtt autoSpecifierP attributeNameP multipleSpecifierP (opt $ defaultP stringP) descriptionP |])
    <|> (token "BOOLEAN" *> [| BoolAtt autoSpecifierP (opt boolValuesP) attributeNameP multipleSpecifierP (opt $ defaultP booleanP) descriptionP |])
    ) <* semi
  
  implementationDefP : Parser ImplementationDef
  implementationDefP = AttributeDef <$> implAttrDefP <|> ReferenceDef <$> implRefDefP

implementationSpecP : Parser ImplementationSpec
implementationSpecP = [| MkImplementationSpec objectP (braces $ some implementationDefP) descriptionP |] <* semi

implementationDefinitionP : Parser ImplementationDefinition
implementationDefinitionP = token "IMPLEMENTATION" *> [| MkImplementationDefinition nameP (braces $ nelP implementationSpecP) descriptionP |]  <* semi

export oilFileP : Parser OilFile
oilFileP = [| MkOilFile oilVersionP implementationDefP applicationDefP |]
